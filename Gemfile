# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

# Rails
gem 'rails', '~> 5.2.2'

# Base gems
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'

# Authentication
gem 'devise', '~> 4.5'

# Front end gems
gem 'bootstrap', '~> 4.0.0.alpha3'
gem 'coffee-rails', '~> 4.2'
gem 'haml-rails', '~> 1.0'
gem 'jquery-rails'
gem 'sass-rails', '~> 5.0'
gem 'simple_form', '~> 4.1'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

# Redis
gem 'redis', '~> 4.0', '>= 4.0.3'

# Database gem setup
gem 'faker', '~> 1.6', '>= 1.6.6'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  gem 'factory_bot_rails', '~> 4.11.1'
  gem 'pry', '~> 0.12.2'
  gem 'rspec-rails', '~> 3.8.1'
  gem 'rubocop', '~> 0.60.0', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
