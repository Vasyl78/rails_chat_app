# frozen_string_literal: true

User.create(
  email: 'admin@test.com',
  password: 'password',
  first_name: Faker::Name.first_name,
  second_name: Faker::Name.last_name,
  admin: true
)
