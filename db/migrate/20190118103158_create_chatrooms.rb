# frozen_string_literal: true

class CreateChatrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :chatrooms do |t|
      t.string :topic
      t.string :type

      t.timestamps
    end
  end
end
