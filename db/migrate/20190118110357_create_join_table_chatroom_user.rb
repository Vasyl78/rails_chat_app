# frozen_string_literal: true

class CreateJoinTableChatroomUser < ActiveRecord::Migration[5.2]
  def change
    create_join_table :chatrooms, :users do |t|
    end
  end
end
