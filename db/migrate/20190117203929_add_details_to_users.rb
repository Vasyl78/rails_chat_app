# frozen_string_literal: true

class AddDetailsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :first_name,  :string
    add_column :users, :second_name, :string
    add_column :users, :admin,       :boolean, default: false
  end
end
