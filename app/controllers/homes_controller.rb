# frozen_string_literal: true

class HomesController < ApplicationController
  def home
    redirect_to admin_home_path if current_user&.admin?
  end
end
