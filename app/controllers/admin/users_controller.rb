# frozen_string_literal: true

module Admin
  class UsersController < AdminController
    before_action :find_user, only: %i[show edit update destroy]
    def index
      @users = User.all
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)
      if @user.save
        redirect_to admin_user_path(@user)
      else
        render :new
      end
    end

    def update
      if @user.update(user_params)
        redirect_to admin_user_path(@user)
      else
        render :edit
      end
    end

    def destroy
      redirect_to admin_users_path, notice: 'User was deleted' if @user.delete
    end

    private

    def user_params
      params.require(:user).permit(:email, :first_name, :second_name, :password)
    end

    def find_user
      @user = User.find(params[:id])
    end
  end
end
