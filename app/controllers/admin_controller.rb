# frozen_string_literal: true

class AdminController < ActionController::Base
  layout 'admin/application'

  before_action :authenticate_user!
  before_action :check_admin_access

  private

  def check_admin_access
    redirect_to root_path unless current_user.admin?
  end
end
