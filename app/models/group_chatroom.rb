# frozen_string_literal: true

class GroupChatroom < Chatroom
  validates :topic, presence: true, uniqueness: true, case_sensitive: false
end
