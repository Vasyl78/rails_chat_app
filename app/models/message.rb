# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :chatroom
  belongs_to :user

  has_many_attached :files

  validates_presence_of :body, :title
end
