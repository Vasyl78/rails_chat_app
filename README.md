# Rails Chat App

## Setup
```sh
$ git clone git@bitbucket.org:Vasyl78/rails_chat_app.git
$ cd rails_chat_app
$ rvm use ruby-2.5.3
# If you do not have installed this version of ruby please install it.
$ rvm install 2.5.3 # or $ rvm install ruby-2.5.3
$ bundle
```
```sh
$ rails db:setup
```
```sh
$ rails s
```

## Description

  * Database PostgreSQL.
