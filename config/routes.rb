# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, skip: :registrations
  root  'homes#home'
  namespace :admin do
    resources :users
    get 'home/', to: 'homes#home'
  end
end
